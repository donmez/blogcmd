


> Written with [StackEdit](https://stackedit.io/).

**Maandag 4 september**
Maandag 4 september hebben we onze dag gestart met een hoorcollege over design en daarna in onze tussenuur hadden we een vergadering met onze team over onze ideeen en wat we allemaal gaan doen en over een taakverdeling.


**Dinsdag 5 september**
In onze eerste Design Challenge les hebben we nog meer nagedacht over onze game ideen en zijn we tot een leuk idee uitgekomen die dag hebben we ook onze eerste feedback gekregen, we hebben een docent gevraagt voor een feedback verder die dag zijn we ook begonnen met het opstellen van een enquette voor een onderzoek naar onze doelgroep en hoe we onze spel kunnen maken, de enquette hebben we op 6 september verder uitgewerkt en afgemaakt. Donderdag 7 september gaan wij naar onze doelgroep en de enquettes laten invullen


**Donderdag 7 september**
Vandaag hadden we geen les maar we hebben 11 uur met onze projectgroep afgesproken om enquettes/intervieuws af te leggen op het wdka. De enquêtes hebben ons heel erg geholpen met het vinden van de juiste idee voor ons game en over wat onze doelgroep leuk vind. We zijn ook de stad in geweest en naar een art gallerie om foto’s te maken over onze thema.



**Vrijdag 8 september**
Vandaag hebben we verder gewerkt aan onze onderzoek en idee voor het spel en ik heb een  beetje geschets van hoe het spel eruit komt te zien. We hebben een workshop over moodboards gehad en over blogs


**Zaterdag 9 september**
Vandaag heb ik een prototype getekend van de app



**Maandag 11 september**
Vandaag hebben we een
hoorcollege gehad en daarna hebben we met onze groep besproken hoe we deze week gaan aanpakken en hebben we verder gewerkt aan onze game. 2 personen zijn vandaag de stad in gegaan om foto te maken bij de locaties die wij hebben voor onze game en ik en twee anderen zijn op school bezig geweest met de puzzel en gekeken waar op het wodka we de puzzel konden plaatsen.
Het hoorcollege vandaag was wel heel leerzaam we hebben heel veel nieuwe dingen te horen gekregen.

